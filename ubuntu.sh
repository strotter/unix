#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

sudo apt-get update
sudo apt-get -y upgrade

sudo apt-get -y install \
	build-essential \
	bison \
	flex \
	gdb \
	openssh-server \
	subversion \
	vim \
	wget \
	m4 \
	lzip \
	python3 \
	python3-pip \
	gccgo \
	golang-go \
	meld \
	terminator \
	xfce4

sudo apt-get -y install \
	 apt-transport-https \
	 ca-certificates \
	 curl

# Add docker repositories
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"

# Add virtualbox repositories
#echo "deb http://download.virtualbox.org/virtualbox/debian yakkety contrib" | sudo tee -a /etc/apt/sources.list
#echo "deb http://cz.archive.ubuntu.com/ubuntu yakkety main universe" | sudo tee -a /etc/apt/sources.list
#wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
#wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -

sudo apt-get update

sudo apt-get -y install docker-ce

sudo gpasswd -a ${USER} docker

sudo service docker restart

mkdir ~/bin

echo "exoprt PATH=/home/${USER}/bin:\$PATH" >> ~/.bashrc

cp ${DIR}/keys.sh /home/${USER}/bin
/home/${USER}/bin/keys.sh
echo "/home/${USER}/bin/keys.sh" >> ~/.bashrc
echo 'export CFLAGS="-O0 -g3"' >> ~/.bashrc
echo 'export CXXFLAGS="-O0 -g3"' >> ~/.bashrc

source ~/.bashrc
