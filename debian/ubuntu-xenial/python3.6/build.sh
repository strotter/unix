ROOT=`pwd`

mkdir -p python-ubuntu-3.6.2/DEBIAN

git clone https://github.com/python/cpython.git source
cd source
git checkout 3.6
git pull

./configure --prefix=${ROOT}/python-ubuntu-3.6.2/usr/local --enable-optimizations --with-ensurepip=install --enable-shared
make -j6
make -j6 test
make install

cd ${ROOT}

cp control python-ubuntu-3.6.2/DEBIAN
dpkg-deb --build python-ubuntu-3.6.2
