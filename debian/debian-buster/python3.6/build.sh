ROOT=`pwd`

mkdir -p python-debian-3.6.2/DEBIAN

sudo apt-get build-dep -y python3.5

git clone https://github.com/python/cpython.git source
cd source
git checkout 3.6
git pull

./configure --prefix=${ROOT}/python-debian-3.6.2/usr/local --enable-optimizations --with-ensurepip=install --enable-shared
make -j6
make -j6 test
make install

cd ${ROOT}

cp control python-debian-3.6.2/DEBIAN
dpkg-deb --build python-debian-3.6.2
